# Step project "FORKIO"

##Overview
This is the webpage of step project "Forkio".
Made by DAN.IT students of the "Front-End" Course.

###Used technologies:

    1. Technologies for adaptive and responsive webpage construction
    2. Node.js / NPM / Gulp packages
    3. Webstorm IDE

###Participants of the project:
    1. Oleksandr Zayats
    2. Roman Zayats

###Tasks of the participants:

    - Gulpfile.js - made by O.Z, R.Z.
    - "Header" section - made by O.Z.
    - "Ratings" section - made by R.Z.
    - "Features" section - made by R.Z.
    - "References" section - made by O.Z.
    - "Prices" section - made by R.Z.